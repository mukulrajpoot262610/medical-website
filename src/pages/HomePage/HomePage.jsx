import React from "react";
import styled from "styled-components";
import Card from "../../Components/Card";
import Caroseul from "../../Components/Caroseul";
import Footer from "../../Components/Footer";
import Header from "../../Components/Header";
import MainNavbar from "../../Components/Navbar/MainNavbar";
import "./HomePage.css";

class HomePage extends React.Component {
  constructor() {
    super();

    this.state = {
      data: [
        {
          id: 1,
          heading: "Book Appointment",
          dcp: "Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, ",
        },
        {
          id: 2,
          heading: "Lab Tests",
          dcp: "Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, ",
        },
        {
          id: 3,
          heading: "Buy Medicines",
          dcp: "Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, ",
        },
        {
          id: 4,
          heading: "Health Insurance",
          dcp: "Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, ",
        },
      ],
    };
  }

  render() {
    return (
      <Home className="homepage">
        <MainNavbar />
        <Header
          h1="Look For Doctors, Test Labs And Pharmacies Near You"
          src="/images/homepage/hero.png"
        />
        <Services>
          <span></span>
          <h1>Our Services</h1>
          <span></span>
        </Services>
        <div className="homepage__cards">
          {this.state.data.map((card) => (
            <Card heading={card.heading} key={card.id} dcp={card.dcp} />
          ))}
        </div>
        <div className="homepage__slider">
          <Caroseul
            img="/images/homepage/cough.png"
            head="Cough"
            para="Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, "
          />
          <Caroseul
            img="/images/homepage/fever.png"
            head="Fever"
            para="Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, "
          />
          <Caroseul
            img="/images/homepage/muscle.png"
            head="Muscle Pain"
            para="Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur adi elitLorem ipsum dolor sit amet, "
          />
        </div>
        <Footer />
      </Home>
    );
  }
}

const Home = styled.div`
  background: url("/images/homepage/bg.png");
  background-position: top;
  background-repeat: no-repeat;
  background-size: contain;
  min-height: 100vh;
`;

const Services = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    width: 11.3rem;
    height: 0px;
    border: 0.3rem solid #04a04a;
  }

  h1 {
    margin: 0 5rem;
    text-align: center;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 4.8rem;
    line-height: 5.9rem;

    color: #04a04a;
  }
`;

export default HomePage;
