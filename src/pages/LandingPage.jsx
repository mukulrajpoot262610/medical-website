import React from "react";
import styled from "styled-components";
import Navbar from "../Components/Navbar/Navbar";
import { Link } from "react-router-dom";

function LandingPage() {
  return (
    <Landing className="landing-page">
      <Navbar />
      <Wrapper>
        <HeroText>
          <h1 className="bigger">Mental health matters:</h1>
          <h4 className="bigger">
            People matter,
            <br />
            You matter, <br />I can’t fight my battle alone, and you don’t have
            to either.
          </h4>
          <h2 className="bigger">REACH OUT</h2>
          <h6 className="smaller heading">
            Look For Doctors, Test Labs And Pharmacies Near You
          </h6>
          <h5 className="smaller subheading">
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam,
          </h5>
          <Link to="/home">
            <img src="/images/button2.png" alt="" />
          </Link>
        </HeroText>
        <HeroImage>
          <img src="/images/hero.svg" alt="" className="hero" />
        </HeroImage>
      </Wrapper>
    </Landing>
  );
}

const Landing = styled.div`
  height: 100vh;
  background: url("/images/bg.png");
  background-size: contain;
  background-position: bottom;
  background-repeat: no-repeat;

  @media (max-width: 768px) {
  }
`;

const Wrapper = styled.div`
  max-width: 132.8rem;
  margin: 0 auto;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  min-height: 80vh;
  padding: 4rem;

  @media (max-width: 768px) {
    flex-direction: column;

    .hero {
      height: 40vh;
    }
  }
`;

const HeroText = styled.div`
  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 3.2rem;
    line-height: 3.9rem;
    display: flex;
    align-items: center;
    margin-bottom: 2rem;

    color: #039348;
  }

  h2 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 900;
    font-size: 24px;
    line-height: 29px;
    display: flex;
    align-items: center;
    text-align: center;
    padding: 20px;
    padding-left: 10rem;
    color: #039348;
  }

  h4 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 24px;
    line-height: 29px;
    display: flex;
    align-items: center;
    margin-bottom: 2rem;
    color: #039348;
  }

  img {
    width: 22.6rem;
    margin-bottom: 4rem;
  }

  .smaller {
    display: none;
  }

  @media (max-width: 500px) {
    .bigger {
      display: none;
    }

    .smaller {
      display: flex;
    }

    h6 {
      margin-top: 2rem;
      font-family: Montserrat;
      font-style: normal;
      font-weight: 600;
      font-size: 3.8rem;
      /* line-height: 2.5rem; */
      display: flex;
      align-items: center;
      text-align: center;
      color: #039348;
    }

    h5 {
      font-family: Montserrat;
      font-style: normal;
      font-weight: normal;
      font-size: 2rem;
      /* line-height: 1.2rem; */
      margin: 2rem 0 4rem 0;
      display: flex;
      align-items: center;
      text-align: center;

      color: #039348;
    }
  }
`;
const HeroImage = styled.div`
  img {
    width: 100%;
  }
`;

export default LandingPage;
