import React from "react";
import NavbarAuth from "../../Components/Navbar/NavbarAuth";
import RenderPage from "../../Components/RenderPage";

function BookApp() {
  return (
    <div>
      <NavbarAuth />
      <RenderPage
        h1="Consult A Doctor Online Or Book An Appointment."
        image="/images/bookhero.png"
        cardData=""
        searchText="Search Doctor near you"
      />
    </div>
  );
}

export default BookApp;
