import React from "react";
import styled from "styled-components";
import Footer from "../../Components/Footer";
import Header from "../../Components/Header";
import MainNavbar from "../../Components/Navbar/MainNavbar";

function AboutPage() {
  return (
    <About className="about">
      <MainNavbar />
      <Header
        h1="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  "
        src="/images/aboutpage/hero.png"
      />
      <Vision>
        <h1>Our Vision</h1>
        <p>
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.""Lorem ipsum
          dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
          velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
          occaecat cupidatat non proident, sunt in culpa qui officia deserunt
          mollit anim id est laborum."
        </p>
      </Vision>
      <Footer />
    </About>
  );
}

const About = styled.div`
  background: url("/images/homepage/bg.png");
  background-position: top;
  background-repeat: no-repeat;
  background-size: contain;
  min-height: 100vh;
`;

const Vision = styled.div`
  max-width: 1229px;
  margin: 0 auto;
  min-height: 60vh;
  padding: 4rem;
  & > h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 3.2rem;
    line-height: 3.9rem;
    display: flex;
    align-items: center;
    color: #039348;
  }

  & > p {
    margin-top: 4rem;
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 2.4rem;
    line-height: 2.9rem;
    display: flex;
    align-items: center;

    color: #039348;
  }
`;

export default AboutPage;
