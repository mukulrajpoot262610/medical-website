import React from "react";
import styled from "styled-components";
import Header from "../../Components/Header";
import MainNavbar from "../../Components/Navbar/MainNavbar";

function FeedPage() {
  return (
    <Feed>
      <MainNavbar />
      <Header
        h1="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore  "
        src="/images/feedpage/hero.png"
      />
    </Feed>
  );
}

const Feed = styled.div`
  background: url("/images/homepage/bg.png");
  background-position: top;
  background-repeat: no-repeat;
  background-size: contain;
  min-height: 100vh;
`;

export default FeedPage;
