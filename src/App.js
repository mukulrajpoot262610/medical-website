import "./App.css";
import LandingPage from "./pages/LandingPage";
import HomePage from "./pages/HomePage/HomePage";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AboutPage from "./pages/AboutPage/AboutPage";
import FeedPage from "./pages/FeedPage/FeedPage";
import signinPage from "./pages/signinPage";
import BookApp from "./pages/AfterAuth/BookApp";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/home" component={HomePage} />
        <Route exact path="/about" component={AboutPage} />
        <Route exact path="/feed" component={FeedPage} />
        <Route exact path="/signin" component={signinPage} />
        <Route exact path="/book" component={BookApp} />
      </Switch>
    </Router>
  );
}

export default App;
