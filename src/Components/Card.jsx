import React from "react";
import styled from "styled-components";

function Card(props) {
  return (
    <CardCompo>
      <img src="/images/homepage/doctor.png" alt="" />
      <Info>
        <Circle>
          <h1>Book Now</h1>
        </Circle>
        <h1>{props.heading}</h1>
        <p>{props.dcp}</p>
      </Info>
    </CardCompo>
  );
}

const Circle = styled.div`
  cursor: pointer;
  position: absolute;
  bottom: 90%;
  left: 80%;
  width: 10rem;
  height: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;

  background: linear-gradient(
    34.55deg,
    rgba(0, 0, 0, 0.2) 11.56%,
    rgba(0, 0, 0, 0.5) 91.44%
  );
  box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.25);
  backdrop-filter: blur(5rem);
  /* Note: backdrop-filter has minimal browser support */
  border-radius: 50%;
`;

const Info = styled.div`
  position: relative;
  text-align: right;
  right: 5%;
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
`;

const CardCompo = styled.div`
  /* Rectangle 151 */
  width: 44.5rem;
  height: 23.1rem;

  display: flex;
  justify-content: space-between;
  align-items: center;
  background: linear-gradient(
    94.56deg,
    rgba(0, 182, 87, 0.3) -14.42%,
    rgba(0, 182, 87, 0.8) 100%
  );
  box-shadow: 6px 6px 15px rgba(0, 0, 0, 0.15);
  backdrop-filter: blur(5px);
  border-radius: 10px;

  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 2.4rem;
    line-height: 2.9rem;
    display: flex;
    align-items: center;
    text-align: center;

    color: #ffffff;
  }

  p {
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 1.4rem;
    line-height: 1.9rem;
    display: flex;
    align-items: center;
    margin-top: 2rem;

    color: #ffffff;
  }

  img {
    object-fit: contain;
    object-position: left;
    height: 100%;
    width: 100%;
  }
`;

export default Card;
