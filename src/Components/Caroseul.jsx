import React from "react";
import styled from "styled-components";

function Caroseul(props) {
  return (
    <Caros>
      <img src={props.img} alt="" />
      <h1>{props.head}</h1>
      <p>{props.para}</p>
      <button>Consult</button>
    </Caros>
  );
}

const Caros = styled.div`
  padding: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;

  margin: 2rem;
  width: 34rem;
  height: 45.3rem;
  background: linear-gradient(
    94.56deg,
    rgba(0, 182, 87, 0.3) -14.42%,
    rgba(0, 182, 87, 0.8) 100%
  );
  box-shadow: 6px 6px 15px rgba(0, 0, 0, 0.15);
  backdrop-filter: blur(5px);
  border-radius: 1rem;

  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 3.6rem;
    line-height: 4.4rem;
    display: flex;
    align-items: center;
    text-align: center;

    color: #ffffff;
  }

  img {
    width: 12rem;
    height: 12rem;
  }

  p {
    font-family: Open Sans;
    font-style: normal;
    font-weight: normal;
    font-size: 1.4rem;
    line-height: 1.9rem;
    display: flex;
    align-items: center;
    /* padding: 0rem 3rem; */

    color: #ffffff;
  }

  button {
    background: linear-gradient(93.74deg, #ffffff 3.02%, #ffffff 100%);
    box-shadow: 0.6rem 0.6rem 1.5rem rgba(0, 0, 0, 0.15);
    backdrop-filter: blur(5px);
    border-radius: 1rem;
    cursor: pointer;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 1.8rem;
    line-height: 2.2rem;
    display: flex;
    align-items: center;
    text-align: center;
    outline: none;
    border: none;
    padding: 1rem 2rem;

    color: #04a04a;
  }
`;

export default Caroseul;
