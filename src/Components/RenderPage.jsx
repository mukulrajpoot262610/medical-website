import React from "react";
import styled from "styled-components";
import Card from "./Card";

function RenderPage(props) {
  return (
    <Page>
      <h1>{props.h1}</h1>
      <Wrapper>
        <Left>
          <Form>
            <Location>
              <section>
                <i class="fas fa-map-marker-alt"></i>
                <input placeholder="Location" />
              </section>
            </Location>
            <Search>
              <section>
                <i class="fas fa-search"></i>
                <input placeholder={props.searchText} />
              </section>
            </Search>
          </Form>
          <RenderCard>
            <Card />
            <Card />
            <Card />
            <Card />
          </RenderCard>
        </Left>
        <Right>
          <img src={props.image} alt="" />
        </Right>
      </Wrapper>
    </Page>
  );
}

const Page = styled.div`
  padding: 4rem;
  max-width: 80%;
  margin: 0 auto;
  min-height: 90vh;

  & > h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    font-size: 4.2rem;
    line-height: 5.1rem;
    display: flex;
    align-items: center;
    color: #04a04a;
  }
`;

const Wrapper = styled.div`
  width: 100%;
  min-height: 76vh;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 1024px) {
    flex-direction: column-reverse;

    & > div {
      width: 100%;
    }
  }
`;

const Left = styled.div`
  width: 70%;
  min-height: 100%;
`;
const Right = styled.div`
  width: 30%;
  min-height: 30vh;
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    position: absolute;
    width: 37.4rem;
    height: 37.4rem;
  }
`;

const Form = styled.form`
  width: 100%;
  min-height: 10vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;

  @media (max-width: 1024px) {
    justify-content: center;
  }
`;

const Location = styled.div`
  section {
    display: flex;
    align-items: center;
    text-align: center;
    font-size: 2rem;
    margin-right: 4rem;

    i {
      margin-right: 1rem;
      color: #04a04a;
    }

    input {
      padding: 2rem;
      text-align: left;
      outline: none;
      border: none;
      border-bottom: 2px solid #04a04a;
      background: none;
      width: 19rem;
      height: 2.5rem;
      font-family: Montserrat;
      font-style: normal;
      font-weight: 500;
      font-size: 1.8rem;
      line-height: 2.2rem;

      color: #04a04a;

      opacity: 0.66;
    }
  }
`;

const Search = styled.div`
  section {
    display: flex;
    align-items: center;
    text-align: center;
    font-size: 2rem;
    margin-right: 4rem;

    i {
      margin-right: 1rem;
      color: #04a04a;
    }
    input {
      padding: 2rem;
      text-align: left;
      outline: none;
      border: none;
      border-bottom: 2px solid #04a04a;
      background: none;
      max-width: 49rem;
      height: 2.5rem;
      font-family: Montserrat;
      font-style: normal;
      font-weight: 500;
      font-size: 1.8rem;
      line-height: 2.2rem;

      color: #04a04a;

      opacity: 0.66;
    }
  }
`;

const RenderCard = styled.section`
  display: grid;
  place-items: center;
  grid-template-columns: 1fr 1fr;
  grid-gap: 5rem;
  max-width: 950px;
  margin: 5rem auto;

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

export default RenderPage;
