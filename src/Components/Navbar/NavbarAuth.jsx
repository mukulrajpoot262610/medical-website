import React from "react";
import styled from "styled-components";

function NavbarAuth() {
  return (
    <Navbar>
      <Logo>
        <img src="/images/logo.png" alt="Logo" />
      </Logo>
      <i className="fas fa-bars"></i>
      <NavLinks className="navlinks">
        <li>
          <a href="/book">Book Appointment</a>
        </li>
        <li>
          <a href="/">Buy Medicine</a>
        </li>
        <li>
          <a href="/">Book Lab Tests</a>
        </li>
        <li>
          <a href="/">Health Insurance</a>
        </li>
        <BtnLinks>
          <a href="/">
            <img src="/images/navbar/e.png" alt="" />
          </a>
          <a href="/signin">
            <img src="/images/avatar.png" alt="" />
          </a>
        </BtnLinks>
      </NavLinks>
    </Navbar>
  );
}

const Navbar = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 10vh;
  padding: 0 3rem;

  background: linear-gradient(
    93.74deg,
    #ffffff 1.77%,
    rgba(255, 255, 255, 0.3) 100%
  );
  box-shadow: 10px 10px 20px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(10px);
  border-radius: 0px 0px 40px 40px;

  i {
    display: none;
    color: #039348;
    font-size: 4rem;
  }

  @media screen and (max-width: 768px) {
    i {
      display: flex;
    }
  }
`;

const Logo = styled.div`
  img {
    height: 5vh;
  }
`;

const BtnLinks = styled.div`
  img {
    height: 6rem;
  }
`;

const NavLinks = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  li {
    list-style: none;
    margin: 0 2rem;

    a {
      text-decoration: none;

      font-family: Montserrat;
      font-style: normal;
      font-weight: 600;
      font-size: 1.4rem;
      line-height: 2rem;
      display: flex;
      align-items: center;
      text-align: center;
      color: #04a04a;
    }
  }

  img {
    cursor: pointer;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export default NavbarAuth;
