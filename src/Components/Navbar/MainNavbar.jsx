import React from "react";
import styled from "styled-components";

function MainNavbar() {
  return (
    <Navbar>
      <Logo>
        <img src="/images/logo.png" alt="Logo" />
      </Logo>
      <i className="fas fa-bars"></i>
      <NavLinks className="navlinks">
        <li>
          <a href="/home">Home</a>
        </li>
        <li>
          <a href="/about">About Us</a>
        </li>
        <li>
          <a href="/feed">Feed</a>
        </li>
        <li>
          <a href="/">Contact Us</a>
        </li>
        <BtnLinks>
          <a href="/">
            <img src="/images/navbar/e.png" alt="" />
          </a>
          <a href="/signin">
            <img src="/images/navbar/l.png" alt="" />
          </a>
        </BtnLinks>
      </NavLinks>
    </Navbar>
  );
}

const Navbar = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 10vh;
  padding: 0 3rem;

  background: linear-gradient(
    93.74deg,
    #ffffff 1.77%,
    rgba(255, 255, 255, 0.3) 100%
  );
  box-shadow: 10px 10px 20px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(10px);
  border-radius: 0px 0px 40px 40px;

  i {
    display: none;
    color: #039348;
    font-size: 4rem;
  }

  @media screen and (max-width: 768px) {
    i {
      display: flex;
    }
  }

  @media screen and (max-width: 500px) {
    box-shadow: none;
    background: none;
  }
`;

const Logo = styled.div`
  img {
    height: 5vh;
  }
`;

const BtnLinks = styled.div`
  img {
    height: 7rem;
  }
`;

const NavLinks = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  li {
    list-style: none;
    margin: 0 3rem;

    a {
      text-decoration: none;

      font-family: Montserrat;
      font-style: normal;
      font-weight: 600;
      font-size: 1.6rem;
      line-height: 2rem;
      display: flex;
      align-items: center;
      text-align: center;
      color: #04a04a;
    }
  }

  img {
    cursor: pointer;
  }

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export default MainNavbar;
