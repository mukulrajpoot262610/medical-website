import React from "react";
import styled from "styled-components";

function Navbar() {
  return (
    <Nav>
      <Logo>
        <img src="/images/logo.png" alt="Logo" />
      </Logo>
      <NavLinks>
        <Contact>
          <img src="/images/contact.png" alt="Call" />
          <h1>Contact</h1>
        </Contact>
        <Emergency>
          <img src="/images/e.png" alt="Emergency" />
          <h1>Emergency</h1>
        </Emergency>
        <Register>
          <a href="/signin">
            <img src="/images/button.png" alt="" />
          </a>
        </Register>
      </NavLinks>
    </Nav>
  );
}

const Nav = styled.nav`
  padding: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  /* background-color: red; */
  height: 10vh;
  max-width: 1328px;
  margin: 0 auto;
`;

const Logo = styled.div`
  img {
    height: 5vh;
  }
`;

const NavLinks = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    cursor: pointer;
  }
`;

const Contact = styled.div`
  margin: 0 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  img {
    height: 5rem;
  }

  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 1.1rem;
    line-height: 1rem;
    display: flex;
    align-items: center;
    text-align: center;

    color: #04a04a;
  }
`;
const Emergency = styled.div`
  margin: 0 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  img {
    height: 5rem;
  }

  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: normal;
    font-size: 1.1rem;
    line-height: 1rem;
    display: flex;
    align-items: center;
    text-align: center;

    color: #d7443e;
  }
`;
const Register = styled.div`
  margin: 0 2rem;
  img {
    width: 19.2rem;
  }
`;

export default Navbar;
