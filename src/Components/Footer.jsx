import React from "react";
import styled from "styled-components";

function Footer() {
  return (
    <Foot>
      <img src="/images/logo.png" alt="" />
    </Foot>
  );
}

const Foot = styled.footer`
  position: relative;
  height: 40vh;
  background: url("/images/homepage/footer.png");
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;

  img {
    position: absolute;
    top: 50%;
    left: 20%;
    height: 10rem;
  }
`;

export default Footer;
