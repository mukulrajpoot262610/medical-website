import React from "react";
import styled from "styled-components";

function Header(props) {
  return (
    <HeaderHero>
      <HeroText>
        <h1>{props.h1}</h1>
      </HeroText>
      <HeroImage>
        <img src={props.src} alt="" />
      </HeroImage>
    </HeaderHero>
  );
}

const HeaderHero = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1328px;
  margin: 0 auto;
  padding: 2rem;

  @media (max-width: 568px) {
    flex-direction: column;
  }
`;

const HeroText = styled.div`
  h1 {
    font-family: Montserrat;
    font-style: normal;
    font-weight: 600;
    font-size: 3.2rem;
    line-height: 3.9rem;
    display: flex;
    align-items: center;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    max-width: 50vh;
    margin-top: 2rem;
    /* background-color: red; */
    color: #039348;
  }
`;

const HeroImage = styled.div`
  img {
    height: 50rem;
  }
`;

export default Header;
